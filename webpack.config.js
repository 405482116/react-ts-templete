const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const isProduction = process.env.NODE_ENV === 'production';

module.exports = {
    entry: './src/index.tsx',
    mode: isProduction ? 'production' : 'development', // production mode minifies the code
    devtool: !isProduction ? 'source-map' : false, // We generate source maps only for development
    output: {
        path: path.join(__dirname, '/dist'),
        filename: '[name].[hash].js',
        chunkFilename: 'chunk.[name].[hash].js',

    },
    module: {
        rules: [
            {
                test: /\.(tsx|ts)$/,
                exclude: /node_modules/,
                use: 'ts-loader',
            }
        ]
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json', '.css'],
        modules: [path.resolve(__dirname, 'src'), 'node_modules']
        // Here we add the extensions we want to support
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'React typescript templete',
            template: './public/index.html',
        }),
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: [path.join(__dirname, 'dist')]
        }),

        new webpack.DefinePlugin({
            TIME_STAMP: new Date().getTime()
        })

    ],
    optimization: {
        // This is to split our bundles into vendor and main
        splitChunks: {
            cacheGroups: {
                default: false,
                commons: {
                    test: /node_modules/,
                    name: 'vendor',
                    chunks: 'all'
                }
            }
        }
    }
};
import * as React from 'react';
import { createRoot } from 'react-dom/client';
import { Hello } from './components/Hello';

const container = document.getElementById('root');
const root = createRoot(container!);

const App = () => {
	return <Hello compiler='TypeScript' framework='React' />;
};

root.render(<App />);
